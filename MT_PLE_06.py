---------- GABARITO MACHINE TEACHING 6 ----------

#1
def altera_frase(frase, palavra, index):
    """função substitui palavra na posição desejada.
    Palavra repetida é coloca em maiúscula, palavra não
    repetida é inserida na posição desejada.
    str, str, int--> str"""

    lista1 = frase.split()  #quebra a frase e transforma a frase em lista
    
    if palavra in lista1:
        lista1[lista1.index(palavra)] = str.upper(palavra)  #rastreia posição da palavra na lista e substitui em maiúsculo
        return str.join(' ', lista1)                        #junta a lista 1 e devolve em formato de string 

    else:
        lista1.insert(index, palavra)    #insere palavra na posição desejada dentro da lista1
        return str.join(' ', lista1)     #junta a lista 1 e devolve em formato de string


#2
def faltas(lista):
    """função recebe uma lista, soma as faltas que as listas informam
    e retorna o total de faltas.
    list--> int"""

    faltas = int(lista[0][2][0]) + int(lista[0][2][1]) + int(lista[1][2][0]) + int(lista[1][2][1]) + int(lista[2][2][0]) + int(lista[2][2][1])
    return faltas


#3
def insere(lista_numero, n):
    """função recebe uma lista, adiciona o elemento n à lista e a ordena
    retorna uma lista
    list, int--> list"""

    lista = lista_numero.insert(-1, n)  #insere n na penúltima posição da lista
    list.sort(lista)                   #organiza a lista em ordem crescente
    return lista 


#4
def maiores(lista, n):
    """função recebe lista e número inteiro e retorna
    lista com elementos da entrada a partir do número inteiro
    list, int--> list"""
    
    if n in lista:                #se n estiver na lista
        list.sort(lista)           #organiza a lista em ordem crescente
        lista1 = lista[list.index(lista, n) + 1:]     #fatia lista - do elemento n até o último elemento e armazena em lista1
        return lista1       #retorna lista fatiada
        
    else:
        lista.insert(-1, n) #insere elemento n na lista na posição de penúltimo elemento
        list.sort(lista)
        lista1 = lista[list.index(lista, n) + 1:]
        return lista1
        

#5
def acima_da_media(lista):
    """função recebe lista e retorna lista ordenada
    list--> list"""
    media = int(sum(lista) / len(lista)) 
    #faz a divisão entre a soma de todos os elementos da lista e o tamanho da lista e atribui  a uma variável qualquer
    #retorna chamando a função maiores e usa como parâmetros a lista e variável qualquer da divisão 
    return maiores(lista, media)


#6
def eh_ordenada(lista):
    """função recebe lista, retorna tupla 
    com true e o tipo de ordem ou retorna tupla
    com false e desordenada
    list--> tuple"""
    
    tupla = ()              
    
    lista1 = lista[:]

    list.sort(lista)
    lista_ordenada = lista[:] 

    list.reverse(lista)
    lista_invertida = lista[:]

    if lista_ordenada == lista1:
        tupla = tupla + (True,'crescente')
        return tupla

    elif lista_invertida == lista1:
        tupla = tupla + (True,'decrescente')
        return tupla

    else:
        tupla = tupla + (False,'desordenada')
        return tupla
    
