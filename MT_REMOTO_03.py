---------- GABARITO MACHINE TEACHING 3 ----------

#1
def PosNegZero(x):
    """função recebe número e informa 
    se o número é positivo, negativo ou zero.
    float--> str"""
    if x > 0:
        return str(x) + ' e positivo'
    if x < 0:
        return str(x) + ' e negativo'
    elif x == 0:
        return str(x) + ' e zero'


#2
def classificacao(cv, ce, cs, fv, fe, fs):
    """informar vitorias, empates e gols de cada time
    int, int, int, int, int, int--> str"""
    
    #ptc = pontos totais cormengo
    #ptf = pontos totais flaminthians
    
    ptc = 3*cv + ce 
    ptf = 3*fv + fe
    
    if (ptc == ptf):
        if (cs > fs):
            return 'Cormengo'
        elif (cs < fs):
            return 'Flaminthians'
        else:
            return 'Empate'
        
    elif (ptc > ptf):
        return 'Cormengo'
    else:
        return 'Flaminthians'


#3
def avioes(c, p_compr, p_compet): 
    total = p_compr/c
    if total >= p_compet:
        return 'Suficiente'
    else: 
        return 'Insuficiente'
    
  
  #  c   p_compr   p_compet
   #10     100        10      suficiente
   #10      90        10     insuficiente
   #5       40         2      suficiente
    
    #100 folhas 
    #33 competidores
