---------- GABARITO MACHINE TEACHING 9 ----------

#1
def eh_quadrada(matriz):
    '''verifica se a matriz é quadrada
    list--> bool'''

    contador = 0  #inicia contador em zero

    if len(matriz) == 0:  #se a matriz não tiver elementos:
        return True  #retornar verdadeiro
    
    for item in matriz:  #para cada item da matriz:
        if len(item) == len(matriz):  #se tamanho do item for igual ao tamanho da matriz:
            contador += 1  #incrementa contador

    if contador == len(matriz):  #se contador for igual ao tamanho da matriz
        return True  #retorna verdadeiro
    else:
        return False  #retorna falso


#2
def conta_numero(numero, matriz):
    '''percorre a matriz inteira e verifica se há número
    int, list--> int'''

    total = 0  #inicia total em zero

    for item in matriz:  #para cada item da matriz:
        for item2 in item:  #para cada item de cada item:
            if item2 == numero:  #se o item do item for igual ao número:
                total += 1  #incrementa total
    
    return total  #retorna total


#3
def media_matriz(matriz):
    '''calcula média de todos os elementos de uma matriz
    e retorna resultado com duas casas de precisão
    list--> float'''

    soma = 0  #inicia contador em zero
    contador = 0  #inicia contador em zero
    
    for item in matriz:  #para cada item da matriz:
        for item2 in item:  #para cada item dos itens da matriz:
            soma += item2  #soma item
            contador += 1  #incrementa contador

    media = soma/contador  #calcula média da soma dos itens
    
    return round(media, 2)  #retorna media com duas casas de precisão


#4
def melhor_volta(matriz):
	'''função que recebe matriz de voltas 
    com tempos e verifica quem fez a melhor volta, com que
    tempo e em que volta, e retorna tupla com dados nessa ordem
    list--> tuple'''

    lista_tempos = []  #abre lista vazia para tempos
    lista_voltas = [] #abre lista vazia para voltas
    

    for voltas in range(6):  #percorre todas as voltas
        for tempos in range(10):  #percorre todos os tempos de cada volta
            if matriz[voltas][tempos] == min(matriz[voltas]):  #se for o menor tempo da volta:    
                lista_tempos.append(tempos)  #captura os índices dos menores tempos de cada volta

    for i in range(6): #percorre elementos do intervalo para criar lista das voltas
        lista_voltas.append(matriz[i][lista_tempos[i]])

    volta = lista_voltas.index(min(lista_voltas))
    tempo = min(lista_voltas)
    piloto = matriz[volta].index(tempo) + 1
    
    tupla = (volta + 1, tempo, piloto)

    #quem fez a melhor volta, qual melhor tempo, que volta foi
    
    return tupla


#5
def busca(setor, matriz):
	'''função retorna dados de todos os 
    funcionários de determinado setor
    str--> list'''
    
    dados = []
    
    for i in range(len(matriz)):
        if setor in matriz[i]:
            dados.append(matriz[i])

    for j in range(len(dados)):
        if dados[j][2] == setor:
            del dados[j][2]
        

    return dados
