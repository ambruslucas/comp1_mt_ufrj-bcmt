---------- GABARITO MACHINE TEACHING 4 ----------

import math

#len(palavra) serve pra pegar o tamanho da palavra - quantas letras tem
#o símbolo // faz a divisão retornando só o valor inteiro 
#tuplas pegam intervalos de letras das palavras - [letra_inicial:letra_final]
#o símbolo + entre strings concatena(junta) as strings

#2
def substitui(s,x,i):
    '''Substitui letra na posição escolhida.
    Inserir palavra entre aspas.
    str--> str'''
    parte1 = s[0:i] #pega letras da palavra começando da primeira até 1 letra antes da string i

    parte2 = s[i+1:len(s)]  #pega as letras da palavra imediatamente depois da str i até o final da palavra

    return parte1 + x + parte2 #retorna a concatenação de strings


print(substitui('palavra', 'y',3))

#3
def hashtag(s):
    def hashtag(s):
    '''Retorna palabra com # no início, no meio e no fim.
    Inserir palavra entre aspas.
    str-> str '''
    
    caracter_meio = len(s)//2 #pega a letra do meio usando o tamanho da palavra dividido por 2 e pega só a parte inteira do resultado
    
    parte1 = s[0:caracter_meio] #pega as letras da palavra da primeira até a letra antes da do meio
    parte2 = s[caracter_meio:len(s)] #pega as letra da palavra da letra do meio até a última letra
    
    return '#' + parte1 + '#' + parte2 + '#' #retorna a concatenação de strings


#4
def filtra_pares(elementos):
    '''Inserir elementos entre colchetes.
    	Retorna tupla só com elementos pares.
        tuple--> tuple'''

    elementos_pares = ()
    
    if elementos[0] % 2 == 0:
        elementos_pares = elementos_pares + (elementos[0],)
    if elementos[1] % 2 == 0:
        elementos_pares = elementos_pares + (elementos[1],)
    if elementos[2] % 2 == 0:
        elementos_pares = elementos_pares + (elementos[2],)
    if elementos[3] % 2 == 0:
        elementos_pares = elementos_pares + (elementos[3],)
    
    return elementos_pares


#5
def colisao(ret1,ret2):
    '''a funcao colisao recebe duas tuplas com quatro valores inteiros cada uma, representando as 
     coordenadas dos vertices inferior esquerdo e superior esquerdo do primeiro retângulo e do segundo 
     retângulo, nessa ordem, e devolve True se ha colisao entre os 2 retangulos e False, caso contrario.
     tuple, tuple --> bool'''

    if ret1[2]<ret2[0]:
        return False
    elif ret2[2]<ret1[0]:
        return False
    elif ret1[3]<ret2[1]:
        return False
    elif ret2[3]<ret1[1]:
        return False
    else:
        return True


#(0,0,1,1), (0,0,1,1) Saída True
#(0,0,2,2), (1,1,3,3) Saída True
#(0,0,1,1), (2,2,3,3) Saída False


