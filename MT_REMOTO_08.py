---------- GABARITO MACHINE TEACHING 8 ----------

#cada linha foi comentada para auxiliar o estudo detalhado das funções
#recomenda-se o uso dos sites https://w3schools.com/python e https://docs.python.org/pt-br/3.8
#bons estudos

#1
def freq_palavras(frase):
    '''função recebe frase e retorna dicionário com
    quantidade de cada palavra da string.
    str--> dict'''

    lista_palavras = frase.split()  #cria lista com palavras da frase
    dict1 = {}  #cria dicionário vazio
    contador = 0  #inicia contador em zero

    for palavra in lista_palavras:  #para cada palavra na lista de palavras:
        dict1[lista_palavras[contador]] = lista_palavras.count(lista_palavras[contador]) #insere elemento no dicionário
        contador += 1  #incrementa contador

    return dict1  #retorna dicionário


#2
def total(lista, produtos):
    '''função calcula total da lista de compras
    de acordo com os preços do dicionário
    list, dict--> float'''

    contador = 0  #inicia contador em zero
    total = 0  #inicia valor total das compras em zero
    
    for elementos in lista:  #para cada elemento da lista de compras
        if lista[contador] in produtos:  #se elemento da lista de compras estiver disponível no dicionário de produtos
            total += produtos[lista[contador]]  #incrementa total com preço do produto(preço é uma chave do dicionário)
            contador += 1  #incrementa contador

    return round(total, 2)  #retorna valor arredondado pra 2 casas decimais


#3
def lingua_p(palavra):
    '''função que recebe palavra em português e
    retorna palavra traduzida em língua do p.
    str--> str'''
    
    traduzido_p = []  #cria lista vazia para a tradução

    for letra in list(palavra):  #para cada letra da palavra inserida no parâmetro da função:
        if letra in 'aeiouáéíóú':  #se letra for uma vogal com ou sem acento
            traduzido_p.append(letra + 'p' + letra)  #insere p e a vogal na lista
        else:  #se não for vogal
            traduzido_p.append(letra)  #insere letra sem p na lista

    return ''.join(traduzido_p)  #junta elementos da lista para formar a palavra
    

#4
def qtd_divisores(numero):
    '''função que verifica quantos divisores um
    número qualquer tem.
    int--> int'''
    
    contador = 0

    for elemento in range(1, numero+1):  #para cada elemento do intervalo 1 até número inserido no parâmetro da função:
        if numero % elemento == 0:  #se divisão do número pelo elemento der zero:
            contador += 1  #incrementa contador
            
    return contador  #retorna contador
     


#5
def primo(numero):
    '''função que verifica se um número 
    qualquer é primo.
    int--> bool'''
    
    contador = 0  #inicia contador em zero
    
    for elemento in range(1, numero+1):  #para cada elemento do intervalo 1 até número inserido no parâmetro da função:
        if numero % elemento == 0:  #se divisão do número pelo elemento der zero:
            contador += 1  #incrementa contador
            
    if contador > 2:  #se contador for maior que 2(tiver mais que 2 divisores):
        return False  #retorna valor booleano Falso
    else:
        return True  #retorna valor booleano Verdadeiro


#6
def soma_h(N):
    '''função calcula somatório de frações com
    N termos onde cada fração tem denominador igual 
    a sua posição no somatório.
    int--> float'''

    lista_soma = [1]  #começa lista com elemento 1 porque o somatório começa com 1

    for numero in range(2, N+1):  #percorre todos os elementos do intervalo de 2 à N
        lista_soma.append((numero)**-1)  #adiciona em lista elementos que serão somados

    somatorio = sum(lista_soma)  #soma todos os elementos da lista

    return round(somatorio, 2)  #retorna o resultado do somatório
