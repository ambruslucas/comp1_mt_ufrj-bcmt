---------- GABARITO MACHINE TEACHING 5 ----------

#1
def quant_palavras(frase):
    """função que conta palavras de uma frase.
    string -> int"""
    contagem = frase.split(" ") #Cria uma lista com os elementos de frase separados por espaço
                                #As palavras são separadas dessa forma
    
    return len(contagem) #retorna o tamanho da lista com as palavras

#2
def conta_frases(texto):
    """função que recebe texto e conta as frases.
    str--> int"""
	contagem1 = texto.count('.')  #separa texto pelos pontos
	contagem2 = texto.count('!')  #separa texto pelos pontos de exclamação
	contagem3 = texto.count('?') #separa texto pelo ponto de interrogação
	contagem4 = texto.count('...') #conta quantas reticências tem na frase
	
	return contagem1 - contagem4 + contagem2 + contagem3

    #Pra cada reticência haverão duas frases a mais, isso acontece porque a contagem
    #de pontos contabiliza os pontos das reticências também
    #Contamos o número de reticência e tiramos do número de pontos, 
    #assim a contagem é feita sem interferências e não fica adulterada.


#3
def intercala(lista1, lista2):
    """recebe duas listas com três elementos e gera uma terceira lista com 
    elementos das duas intercalados.
    list, list--> list"""
    lista3 = [lista1[0], lista2[0], lista1[1], lista2[1], lista1[2], lista2[2]]

    return lista3


#4
def retira_pontuacao(frase):
    """função que recebe frase e tira as pontuações.
    str--> str"""

    if ":" in frase:
        frase = frase.replace(":", " ")
    if ";" in frase:
        frase = frase.replace(";", " ")
    if "." in frase:
        frase = frase.replace(".", " ")
    if "!" in frase:
        frase = frase.replace("!", " ")
    if "-" in frase:
        frase = frase.replace("-", " ")
    if "," in frase:
        frase = frase.replace(",", " ")
    if "?" in frase:
        frase = frase.replace("?", " ")

    return frase


#4 (alternativa com regex)
import re 
def substitui(texto):
    substituido = re.sub(r'[^\w\s]', '', texto)

    return substituido
    

#5
def inverte(frase):
    """função que recebe frase e inverte a ordem das palavras
    str--> str"""

    if ":" in frase:
        frase = frase.replace(":", "")
    if ";" in frase:
        frase = frase.replace(";", "")
    if "." in frase:
        frase = frase.replace(".", "")
    if "!" in frase:
        frase = frase.replace("!", "")
    if "-" in frase:
        frase = frase.replace("-", " ")
    if "," in frase:
        frase = frase.replace(",", "")
    if "?" in frase:
        frase = frase.replace("?", "")


    fraselist = frase.split(" ")
    range_list = len(fraselist)+1
    fat_inv = fraselist[-1:-(range_list):-1]
    inverso = str.join(' ',fat_inv)

    return str.lower(inverso)


#6
def piramide_num(num1, num2):
    """função que devolve uma lista pirâmide.
       int, int --> list"""

    lista1 = [num1]

    if num1 > num2:
        return list(range(num1, num2, -1)) + list(range(num2, num1)) + lista1
    elif num1 < num2:
        return list(range(num1, num2)) + list(range(num2, num1, -1)) + lista1
    else:
        return lista1


#colchudo
def colchao(medidas, H, L):
    """função define se colchão passa pela porta
    de acordo com as medidas do colchão.
    list, float, float--> bool"""
    
    if medidas[1] <= H:
        return True
    if medidas[1] <= L:
        return True
    if medidas[2] <= H:
        return True
    if medidas[2] <= L:
        return True
    else:
        return False


