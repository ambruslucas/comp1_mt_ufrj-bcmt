---------- GABARITO MACHINE TEACHING 7 ----------

#função insert não dá retorno logo não pode ser atribuída a variável
#contador += 1 é o mesmo que contador = contador + 1

#1
def filtraMultiplos(lista, n):
    """função que recebe lista e retorna lista 
    com números múltiplos do número n
    list, int--> list"""

    multiplos = []  #cria lista vazia para múltiplos
    contador = 0  #inicia o contador em zero

    while contador < len(lista):  #enquanto contador for menor que tamanho da lista
        if lista[contador] % n == 0:  #se elemento da lista for divisível por n
            multiplos.insert(contador, lista[contador])  #insere elemento na posição relacionada ao valor do contador
        contador += 1  #incrementa contador
        
    return multiplos  #retorna lista com múltiplos de n


#2
def uppCons(texto):
    '''função recebe texto e retorna o mesmo texto 
    com as consoantes em maiúsculo.
    str--> str'''
    lista_texto = list(texto)  #quebra a string texto e coloca em uma lista
    contador = 0 #inicia o contador 
    letras = []  #abre uma lista vazia    

    while contador < len(lista_texto):  #enquanto contador for menor que lista com o texto
        if lista_texto[contador] in 'çbcdfghjklmnpqrstvwxyz':  #se elemento da lista com o texto for consoante
            letras.insert(contador, lista_texto[contador].upper())  #insere elemento em maiúsculo na lista letras
            contador = contador + 1  #incrementa contador
        else:  #se não for consoante
            letras.insert(contador, lista_texto[contador])  #insere elemento do texto na lista letras
            contador = contador + 1  

    return ''.join(letras)  #junta os elementos da lista letras e retorna string


#3
#resolução com um contador
def posLetra(frase, letra, numero):
    '''função que retorna posição da letra na ocorrência desejada
    str, str, int--> int'''
    
    letras = list(frase)  
    indices = []
    contador = 0

    while len(letras) > contador:
        if letra in letras[contador]:
            indices = indices + [contador]

        contador += 1

    if len(indices) < numero:
        return -1
    else:
        return indices[numero -1]


#3
#resolução com dois contadores
def posLetra(frase, letra, numero):
    '''função que retorna posição da letra na ocorrência desejada
    str, str, int--> int'''
    
    palavras = list(frase)  #cria lista com todas as letras da frase
    contador = 0  #inicia contador em zero
    indice = 0  #inicia indice em zero
    
    while len(palavras) > contador:  #enquanto tamanho da lista palavras for maior que o contador
        if letra in palavras[contador]:  #se tiver a letra no elemento da lista palavras
            indice += 1  #incrementa numero do índice
            
        if indice == numero:  #quando índice bate no numero
            return contador  #retorna contador
            
        contador += 1  #incrementa contador

    if indice < numero:  #se índice for menor do que ocorrencia desejada
        return -1  #retorna -1


#4
def repetidos(lista_num):
    '''função que conta quantas vezes um elemento apareceu repetido
    list--> int'''

    contador = 1  #inicia o contador em 1
    vezes = 0  #inicia vezes em zero
    
    while len(lista_num) > contador:  #equanto lista de números não for percorrida completamente
        if lista_num[contador] == lista_num[contador - 1]:  #se elemento da lista de número for igual a elemento anterior
            vezes += 1  #incrementa vezes em 1 unidade
            contador += 1  #incrementa contador em 1 unidade
        else:  #senão
            contador += 1  #incrementa contador em 1 unidade
            
    return vezes  #retorna número de vezes


#5
def fatorial(numero):
    """função calcula o fatorial de um 
    número inteiro qualquer
    int--> int"""
    
    contador = 1  #inicia o contador em 1
    fatorial = numero  #armazena numero em variável fatorial
    
    while contador < numero:  #enquanto contador for menor que número
        fatorial = fatorial * contador  #fatorial é igual fatorial vezes contador
        contador = contador + 1  #incrementa contador 

    return fatorial  #retorna resultado do fatorial


#6
def faltante(lista):
    """função recebe lista e retorna elemento que falta
    list--> int"""
    
    list.sort(lista)
    
    lista_ordenada = list(range(1, len(lista)+2))
    
    contador = 0
    
    while len(lista_ordenada) > contador+1:
        if lista[contador] != lista_ordenada[contador]:
            return lista_ordenada[contador]
            
        contador +=1
        
    return lista_ordenada[-1]


